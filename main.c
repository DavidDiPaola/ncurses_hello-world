/* 2020 David DiPaola. Licensed CC0 (public domain worldwide). */

#include <stdio.h>

#include <stdlib.h>

#include <ncurses.h>

static struct {
	WINDOW * window;
	int maxx;
	int maxy;
} _curses;

static void
_quit(int status) {
	int nc_status = endwin();
	if (nc_status == ERR) {
		fprintf(stderr, "endwin() failed" "\n");
	}

	exit(status);
}

static void
_init(void) {
	int status;

	_curses.window = initscr();
	if (!_curses.window) {
		fprintf(stderr, "initscr() failed" "\n");
		exit(1);
	}

	/* enable non-blocking input */
	status = nodelay(_curses.window, TRUE);
	if (status == ERR) {
		fprintf(stderr, "nodelay() failed" "\n");
		_quit(1);
	}
	
	/* disable line buffering */
	status = cbreak();
	if (status == ERR) {
		fprintf(stderr, "cbreak() failed" "\n");
		_quit(1);
	}

	/* disable input echo-ing */
	status = noecho();
	if (status == ERR) {
		fprintf(stderr, "noecho() failed" "\n");
		_quit(1);
	}

	/* enable return key */
	status = nonl();
	if (status == ERR) {
		fprintf(stderr, "nonl() failed" "\n");
		_quit(1);
	}

	/* enable arrow keys */
	status = keypad(_curses.window, TRUE);
	if (status == ERR) {
		fprintf(stderr, "keypad() failed" "\n");
		_quit(1);
	}

	/* disable cursor */
	status = curs_set(0);
	if (status == ERR) {
		fprintf(stderr, "curs_set() failed" "\n");
		_quit(1);
	}
}

static void
_curses_update(void) {
	_curses.maxx = getmaxx(_curses.window);
	_curses.maxy = getmaxy(_curses.window);
}

int
main() {
	_init();

	int done = 0;
	while (!done) {
		_curses_update();

		mvwprintw(_curses.window, /*y=*/0, /*x=*/0, "hello world! press Shift+Q to quit");
		mvwprintw(_curses.window, /*y=*/1, /*x=*/0, "window size: %i x %i", _curses.maxx, _curses.maxy);

		int ch = wgetch(_curses.window);
		switch (ch) {
		case ERR:  /* this occurs when there was no input */
			break;
		case 'Q':
			done = 1;
			break;
		case KEY_UP:
			mvwprintw(_curses.window, /*y=*/3, /*x=*/0, "you pressed UP");
			wclrtoeol(_curses.window);
			break;
		case KEY_DOWN:
			mvwprintw(_curses.window, /*y=*/3, /*x=*/0, "you pressed DOWN");
			wclrtoeol(_curses.window);
			break;
		case KEY_LEFT:
			mvwprintw(_curses.window, /*y=*/3, /*x=*/0, "you pressed LEFT");
			wclrtoeol(_curses.window);
			break;
		case KEY_RIGHT:
			mvwprintw(_curses.window, /*y=*/3, /*x=*/0, "you pressed RIGHT");
			wclrtoeol(_curses.window);
			break;
		default:
			mvwprintw(_curses.window, /*y=*/3, /*x=*/0, "you pressed %c (0x%02X)", ch, ch);
			wclrtoeol(_curses.window);
			break;
		}

		refresh();
	}

	_quit(0);
}
