# 2020 David DiPaola. Licensed CC0 (public domain worldwide).

SRC_C = main.c
BIN = ncurses_hello-world

NCURSES_CFLAGS = -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=600
NCURSES_LDFLAGS_LIB = -Wl,-Bsymbolic-functions -lncurses -ltinfo

_CFLAGS = \
	-std=c99 -fwrapv \
	-Wall -Wextra \
	-g \
	-O2 \
	$(NCURSES_CFLAGS) \
	$(CFLAGS)
_LDFLAGS = \
	-O1 \
	$(LDFLAGS)
_LDFLAGS_LIB = \
	$(NCURSES_LDFLAGS_LIB)

OBJ = $(SRC_C:.c=.o)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -rf $(OBJ) $(BIN)

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(BIN): $(OBJ)
	$(CC) $(_LDFLAGS) $^ -o $@ $(_LDFLAGS_LIB)

